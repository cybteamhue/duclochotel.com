        <div class="bg_color_1">   
           <div class="container margin_80_55">
                    <div class="main_title_2">
                        <span><em></em></span>
                        <h2>TIN TỨC & SỰ KIỆN</h2>
                    </div>
                    <div class="row">
                        @foreach ($posts as $post)
                        <div class="col-lg-6">
                                <a class="box_news" href="{{ route('detail.Blog',$post->slug) }}">
                                    <figure><img src="{{ Voyager::image( method_exists($post, 'thumbnail') ? $post->thumbnail('cropped') : $post->image ) }}" class="img-fluid" alt="">
                                    </figure>
                                    <ul>
                                        <li>{{$post->created_at->format('d-m-Y')}}</li>
                                    </ul>
                                    <h4>{{$post->title}}</h4>
                                    <p>{!! $post->excerpt !!}.</p>
                                 
                                </a>
                            </div>
                            <!-- /box_news -->   
                        @endforeach                                            
                    </div>
                    <!-- /row -->
                    <p class="btn_home_align"><a href="{{ route('blog') }}" class="btn_1 rounded">Xem thêm</a></p>
                </div>
        </div>