<div class="col-lg-8">
    <section id="description" class=" blog_body">
        <div class="detail_title_1">
            <h1>{{$post->title}}</h1>
            <i class=" icon-back-in-time"></i><span>{{$post->created_at->format('d-m-Y')}}</span>
        </div>
        <p>{!!$post->body!!}</p>
        <div class="notice notice-success">
            <strong>CÓ THỂ BẠN QUAN TÂM</strong>            
        </div>

        <div class="row">
                @foreach($posts as $postother)
                    @if ($post->id != $postother->id)
                        <div class="col-lg-6 col-md-12">
                            <ul class="menu_list">
                                <li>
                                    <div class="thumb">
                                        <a href="{{ route('detail.Blog',$postother->slug) }}"> <img src="{{ Voyager::image( method_exists($postother, 'thumbnail') ? $postother->thumbnail('cropped') : $postother->image ) }}" alt="{{$postother->title}}"></a>
                                    </div>
                                    <h6><a href="{{ route('detail.Blog',$postother->slug) }}">{{$postother->title}}</a></h6>
                                    
                                    <p>{{$postother->created_at->format('d-m-Y')}}</p>
                                </li>
                                
                               
                            </ul>
                        </div>
                    @endif
                @endforeach    
        </div>
    </section>
    <!-- /section -->
</div>