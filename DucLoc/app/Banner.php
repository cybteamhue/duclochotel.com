<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Resizable;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    // use SoftDeletes, Resizable;
    protected $fillable = ['name','image','id_information'];
    public function idInformation(){
        return $this->belongsTo(Information::class,'id_information','id');
      }
}
